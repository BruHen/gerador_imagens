<?php

function createImage(
    int $width,
    int $height,
) {
    $imagem = ImageCreate((int)$width,(int)$height); //Cria uma imagem com as dimensões 100x20

    $vermelho = ImageColorAllocate($imagem, 255, 0, 0); //Cria o segundo plano da imagem e o configura para vermelho
    $branco = ImageColorAllocate($imagem, 255, 255, 255); //Cria a cor de primeiro plano da imagem e configura-a para branco
    
    ImageString($imagem, 3, 3, 3, "PHPBrasil", $branco); //Imprime na imagem o texto PHPBrasil na cor branca que está na variável $branco
    return $imagem;
}


function renderImage($image, $type="image/gif") {
    header("Content-type: ${type}"); //Informa ao browser que o arquivo é uma imagem no formato GIF
    ImageGif($image); //Converte a imagem para um GIF e a envia para o browser
    ImageDestroy($image); //Destrói a memória alocada para a construção da imagem GIF.
}

$width = $_GET['width'];
$height = $_GET['height'];
$imagem =  createImage($width, $height);
renderImage($imagem);

           